package com.devalgas.demojgiven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJgivenApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoJgivenApplication.class, args);
	}

}
