package com.devalgas.demojgiven;

import com.tngtech.jgiven.Stage;

/**
 * @author devalgas kamga on 01/02/2022. 18:09
 */
public class ThenSomeOutcome extends Stage<ThenSomeOutcome> {
    public ThenSomeOutcome some_outcome() {
        return self();
    }
}
