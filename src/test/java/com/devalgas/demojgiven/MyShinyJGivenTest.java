package com.devalgas.demojgiven;

import com.tngtech.jgiven.junit.ScenarioTest;
import org.junit.Test;


/**
 * @author devalgas kamga on 01/02/2022. 18:07
 */
public class MyShinyJGivenTest extends ScenarioTest<GivenSomeState, WhenSomeAction, ThenSomeOutcome> {

    @Test
    public void something_should_happen() {
        given().some_state();
        when().some_action();
        then().some_outcome();
    }
}
