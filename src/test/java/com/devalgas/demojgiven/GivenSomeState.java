package com.devalgas.demojgiven;

import com.tngtech.jgiven.Stage;

/**
 * @author devalgas kamga on 01/02/2022. 18:08
 */
public class GivenSomeState extends Stage<GivenSomeState> {
    public GivenSomeState some_state() {
        return self();
    }
}
