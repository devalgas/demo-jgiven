package com.devalgas.demojgiven;

import com.tngtech.jgiven.Stage;

/**
 * @author devalgas kamga on 01/02/2022. 18:08
 */
public class WhenSomeAction extends Stage<WhenSomeAction> {
    public WhenSomeAction some_action() {
        return self();
    }

}
